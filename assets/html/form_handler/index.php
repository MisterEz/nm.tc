<?php
// Handles user form inputs
//
// POST:
//  n:    name
//  e:    email
//  m:    message
//  name: bot-buster (Should be blank)
//
// GET:
//  ajax: is ajax (1/0) (Default: 0)
//
// Example Usage:
//  /form_handler/?ajax=1


///////////////
// Validation
///////////////

$ajax =  (!empty($_GET['ajax']) && $_GET['ajax'] == "true");

// Must be post request method
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    return_fail('NOT_POST');
    
}
// Must have all fields
else if (!isset($_POST['n']) || !isset($_POST['e'])  || !isset($_POST['m']))
{
    return_fail('Form filled out incorrectly');
}
// Bot buster field must be empty
else if (!empty($_POST['name']))
{
    return_fail('Beep boop! Good day Mr. roboto');
}
// Must be a valid email
else if (!filter_var($_POST['e'], FILTER_VALIDATE_EMAIL))
{
    return_fail('Invalid email address');
}
// Name and message must not be empty
else if (empty($_POST['n']) || empty($_POST['m']))
{
    return_fail('This form looks a little bit too empty');
}


///////////////
// Sanitation
///////////////

// Save unsanitized
$unsafe_name    = trim($_POST['n']);
$unsafe_email   = trim($_POST['e']);
$unsafe_message = trim($_POST['m']);

// Save sanitised
$name    = filter_var ($unsafe_name,    FILTER_SANITIZE_STRING);
$email   = filter_var ($unsafe_email,   FILTER_SANITIZE_EMAIL);
$message = filter_var ($unsafe_message, FILTER_SANITIZE_STRING);


///////////////
// Mail
///////////////

$ip = $_SERVER['REMOTE_ADDR'];

//Read config
$ini_array = parse_ini_file('config.ini');

$email_to   = $ini_array['email_to'];
$email_from = $ini_array['email_from'];

$headers = "From: $email_from" . "\r\n" .
"Reply-To: $email" . "\r\n" .
'X-Mailer: PHP/' . phpversion();

$new_message = "Name: $name Email: $email IP: $ip \r\n";
$wrapped_message = wordwrap($new_message . $message, 70, "\r\n");
$ret = mail($email_to, "Contact Form: $name", $wrapped_message, $headers);

if (!$ret)
{
    return_fail('Failed to send email :(');
}

///////////////
// Return
///////////////

if ($ajax)
{
    header('Content-type: application/json');
    echo json_encode(array('success' => true));
}
else
{
    echo "Message Sent! <br><a href='/'>Return</a>";
    echo "<script>setTimeout(function(){window.location.replace('https://nm.tc');}, 3000)</script>";
}

function return_fail($msg) {
    if ($ajax)
    {
        header('Content-type: application/json');
        echo json_encode(array('success' => false, 'msg' => $msg));
    }
    else
    {
        echo "Form Error: $msg <br><a href='/'>Return</a>";
        echo "<script>setTimeout(function(){window.location.replace('https://nm.tc');}, 3000)</script>";
    }
    exit();
}
