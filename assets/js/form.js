var form = document.getElementById("nmform");
var inputs = document.getElementsByClassName("validate_me");
var submit_button = document.getElementById("submit_button");
var botbuster = document.getElementById("bot-buster");

form.addEventListener('submit', do_submit);
for (var i = 0; i < inputs.length; i++)
{
    inputs[i].addEventListener('blur',validate_input);
    inputs[i].addEventListener('keyup', input_change);
}

function validate_input(e, input)
{
    if (input === undefined)
        input = e.target;

    var ret = is_valid(input)

    switch(ret)
    {
        case -1:
            return_msg("This field is required", 1, input.nextElementSibling);
            input.classList.add("invalid");
            break
        case -2:
            return_msg("Invalid emaill address", 1, input.nextElementSibling);
            input.classList.add("invalid");
            break;
        case 0:
            input.classList.remove("invalid");
            return_msg("", 0, input.nextElementSibling)
    }

}
function input_change(e)
{
    var input = e.target;
    if (input.classList.contains('invalid') && is_valid(input) === 0)
    {
        input.classList.remove("invalid");
        return_msg("", 0, input.nextElementSibling)
    }
}
function is_valid(el)
{
    if (el.value.length <= 0)
    {
        return -1;
    }
    if (el.type === "email")
    {
        var re = /\S+@\S+\.\S+/
        if (re.test(el.value) !== true)
        {
            return -2;
        }
    }
    return 0;
}

function return_msg(msg, type, el)
{
    if (el === undefined)
        el = document.getElementById("form_message_return");
    // Clear
    el.innerHTML = "";
    // Add text safely
    el.appendChild(document.createTextNode(msg))
    // Add class
    if (type === 0)
    {
        el.classList.remove('failure');
        el.classList.add('success')
    }
    else
    {
        el.classList.remove('success');
        el.classList.add('failure')
    }
}

function do_submit(e)
{
    e.preventDefault();
    
    var is_valid = true;
    for (var i = 0; i < inputs.length; i++)
    {
        validate_input(null, inputs[i]);
        if (inputs[i].classList.contains("invalid"))
        {
            is_valid = false;
        }
    }
    if (is_valid)
    {
        submit_button.disabled = true;

        var formData = new FormData();
        for(var i=0; i<inputs.length; i++)
        {
            formData.append(inputs[i].name, inputs[i].value);
        }
        formData.append(botbuster.name, botbuster.value);

        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function()
        {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
            {
                if (xmlHttp.response != undefined){
                    var resp = JSON.parse(xmlHttp.response);
                    console.log(resp);
                }
                else
                {
                    return_msg("Unknown server error: Invalid Response", 1);
                    submit_button.disabled = false;
                }

                if (resp.success !== undefined && resp.success == true)
                {
                    return_msg("Message Sent!", 0);
                    
                    for (var i = 0; i < inputs.length; i++)
                    {
                        inputs[i].readOnly = true;
                    }
                }
                else
                {
                    if (resp.msg !== undefined)
                    {
                        return_msg(resp.msg, 1);
                    }
                    else
                    {
                        return_msg("Unknown server error: Invalid Response", 1);
                        submit_button.disabled = false;
                    }
                }
            }
            else if (xmlHttp.readyState == 1)
            {
                return_msg("Sending...", 0);
            }
            else if (xmlHttp.readyState == 4)
            {
                return_msg("Unknown server error", 1);
                submit_button.disabled = false;
            }
        }
        xmlHttp.open("post", "form_handler/?ajax=true"); 
        xmlHttp.send(formData); 
    }
}
