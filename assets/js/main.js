const THREE = require('three');
import 'three-examples/loaders/GLTFLoader';

function webgl_support() {
    try {
        var canvas = document.createElement('canvas');
        return !!window.WebGLRenderingContext &&
            (canvas.getContext('webgl') || canvas.getContext('experimental-webgl'));
    } catch (e) {
        return false;
    }
};
var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

//If no webgl
if (!webgl_support() || isIE11) {
    var loader_img = document.getElementById("canvas_loader");
    loader_img.className = "fullsize";
} else {
    
    var canvas = document.getElementById("happyCanvas");
    var lhs = document.getElementsByClassName("h_lhs")[0];

    // Workaround for chrome/safari window.innerHeight bug with autohiding nav bar
    // use heroScene.clientHeight instead as it is styled with 100vh
    var heroScene = document.getElementsByClassName("heroScene")[0];

    var camera, scene, renderer, planet, clouds;
    var vOffset = 0;
    var spin = false;


    //Breakpoints
    // >1280 Desktop
    // 768-480 Tablet
    // <480 Mobile

    var tablet_width = 768;
    var mobile_width = 480;

    window.isMobile = (/Mobi|Android/i.test(navigator.userAgent));

    init();
    animate();

    function init() {
        scene = new THREE.Scene();

        camera = new THREE.PerspectiveCamera(45, (window.innerWidth) / (heroScene.clientHeight), 0.1, 1000);

        renderer = new THREE.WebGLRenderer({
            canvas: canvas,
            antialias: true,
            alpha: true
        });

        if (window.isMobile) {
            canvas.className = "static";
        }

        var loader = new THREE.GLTFLoader();

        loader.load('assets/3d/happy_planet_5.glb', function (gltf) {
            planet = gltf.scene.children[0];
            clouds = gltf.scene.children[1];

            scene.add(planet);
            planet.material = planet_mat;
            planet.frustumCulled = false;

            if (window.innerWidth > tablet_width && !window.isMobile) {
                scene.add(clouds);
                clouds.material = clouds_mat;
                clouds.frustumCulled = false;
            }
            canvas.className += " cshow";

            setTimeout(function () {
                //Clear loader_img
                var loader_img = document.getElementById("canvas_loader");
                loader_img.style.display = "none";
                spin = true;
            }, 325);


        }, undefined, function (error) {
            console.error("AHHH: " + error);
        });

        var texLoader = new THREE.TextureLoader();

        var map1 = texLoader.load('assets/3d/1024_tiny.jpg');
        map1.flipY = false;
        //Flat material -- no lighting no shading
        var planet_mat = new THREE.MeshBasicMaterial({
            map: map1
        });

        var map2 = texLoader.load('assets/3d/256_80.jpg');
        map2.flipY = false;
        var clouds_mat = new THREE.MeshBasicMaterial({
            map: map2
        });

        camera.position.z = 150;

        /*controls = new THREE.OrbitControls( camera );
        controls.target.set( 0, - 0.2, - 0.2 );
        controls.update();*/
        window.addEventListener('resize', resize, false);
        resize();
    }

    function resize() {
        var lhs_width = lhs.offsetWidth + lhs.offsetLeft;
        camera.aspect = (window.innerWidth) / (heroScene.clientHeight);
        renderer.setSize(window.innerWidth, heroScene.clientHeight);

        if (window.innerWidth <= tablet_width && window.innerHeight > mobile_width) {
            //camera.fov = 45 * (mobile_width / window.innerWidth)
            camera.fov = 60;
        } else {
            camera.fov = 45;
        }

        camera.updateProjectionMatrix();
    }

    //The scene changes as the user scrolls
    function frameToScroll(scroll) {
        // Raise camera against scroll
        // This enhances the parallax and makes the planet look closer
        // This effect doesn't work on mobile however
        if (!window.isMobile)
            camera.position.y = -(scroll / 4);
        else {
            camera.position.y = 0;
        }


    }

    function animate() {

        // Only render if in view
        // this ones for you, captain planet
        if (window.scrollY < heroScene.clientHeight) {
            if (canvas.style.display === "none") {
                canvas.style.display = "block";
            }
            frameToScroll(window.scrollY);

            renderer.render(scene, camera);
            if (planet && spin)
                planet.rotation.y -= 0.003;

        } else if (canvas.style.display !== "none") {
            canvas.style.display = "none";
        }
        requestAnimationFrame(animate);
    }

    /*
    canvas.addEventListener('mousemove', onMouseMove, false);

    function onMouseMove(e) {
        var pos = getMousePos(canvas, e)
        camera.position.x = -pos.x / 100;
        camera.position.y = pos.y / 100;
        camera.lookAt(pos.x / 100, -pos.y / 100, 0);
    }

    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect(),
            scaleX = canvas.width / rect.width,
            scaleY = canvas.height / rect.height;

        return {
            x: (evt.clientX - rect.left - canvas.width / 2) * scaleX,
            y: (evt.clientY - rect.top - canvas.height / 2) * scaleY
        }
    }
    */
}