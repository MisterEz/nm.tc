import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();


// Change header to solid when scrolled from top

var transHeader = true;
var heroCanvas = document.getElementById("happyCanvas");

function headerScroll() {
    if (transHeader && window.scrollY !== 0) {
        transHeader = false;
        document.getElementById("nav-header").className = "nav-solid";
    } else if (!transHeader && window.scrollY === 0) {
        transHeader = true;
        document.getElementById("nav-header").className = "nav-transp";
    }
}
window.addEventListener('scroll', headerScroll);
headerScroll();


// Calc section scrollTop

var sectionHeights = [0, 0, 0];
var about = document.getElementById("about");
var work = document.getElementById("work");
var contact = document.getElementById("contact");

function calcSectionHeights(e) {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

    sectionHeights[0] = top + about.getBoundingClientRect().top - 10;
    sectionHeights[1] = top + work.getBoundingClientRect().top - 10;
    sectionHeights[2] = top + contact.getBoundingClientRect().top - 10;
}
window.addEventListener('resize', calcSectionHeights);
calcSectionHeights()

// Change active header link when scrolling


var about_link = document.getElementById("about_link");
var work_link = document.getElementById("work_link");
var contact_link = document.getElementById("contact_link");

function sectionScroll() {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

    if (top <= sectionHeights[0]) {
        about_link.className = "";
        work_link.className = "";
        contact_link.className = "";
    } else if (top <= sectionHeights[1]) {
        about_link.className = "active";
        work_link.className = "";
        contact_link.className = "";
    }
    else if (top <= sectionHeights[2]) {
        about_link.className = "";
        work_link.className = "active";
        contact_link.className = "";
    }
    else
    {
        about_link.className = "";
        work_link.className = "";
        contact_link.className = "active";
    }
}
window.addEventListener('scroll', sectionScroll);
sectionScroll();

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.getElementById(this.getAttribute('href').substring(1))
            .scrollIntoView({
                alignToTop: true,
                behavior: 'smooth',
                block: "start"
            });

    });
});