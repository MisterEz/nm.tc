## My personal website

See it in action at https://nm.tc

### Building

Assuming you have npm installed

`npm install`

`npm start`

or build with

`npm run build`
