var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var htmlreplace = require('gulp-html-replace');
let cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
var compiler = require('webpack');
const webpack = require('webpack-stream');
var path = require('path');
const del = require('del');
const ClosurePlugin = require('closure-webpack-plugin');
var replace = require('gulp-replace');
var gulpif = require('gulp-if');
const htmlmin = require('gulp-htmlmin');




const build_array = ['sass', 'js', 'copy-3d', 'copy-img', 'html'];

/*
    =========================
    Run commands
    =========================
        gulp development
        gulp production
*/
gulp.task('development', ['clean'], () => {
    gulp.start('serve')
});
gulp.task('production', ['clean'], () => {
    gulp.start(build_array)
});

// Reset build directory
gulp.task('clean', () => {
    return del(['./build']);
});

// Static Server + watching scss/html files
gulp.task('serve', build_array, function () {

    browserSync.init({
        server: "./build",
        ghostMode: false
    });

    gulp.watch("assets/sass/*.scss", ['sass']);
    gulp.watch("assets/sass/critical.scss", ['html']);
    gulp.watch("assets/html/*.html", ['html']);
    gulp.watch("assets/js/*.js", ['js']);
    gulp.watch("assets/3d/*", ['copy-3d']);
    gulp.watch("assets/img/*", ['copy-img']);

    // suicide on change
    gulp.watch("gulpfile.js").on("change", () => process.exit(0));
    browserSync.notify('TEST', 3000);
});


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function () {
    return gulp.src("assets/sass/*.scss")
        .pipe(sass())
        .on('error', handleErrors)
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie10'
        }))
        .pipe(gulp.dest("./build/assets/css"))
        .pipe(browserSync.stream());
});

// Move html to build and htmlReplace to inline stuff
gulp.task('html', ['sass'], function () {
    gulp.src('assets/html/*.html')
        .pipe(htmlreplace({
            cssInline: {
                src: gulp.src('build/assets/css/critical.css').pipe(cleanCSS({
                    compatibility: 'ie10'
                })),
                tpl: '<style>%s</style>'
            }
        }))
        .pipe(gulpif(process.env.NODE_ENV === 'production', htmlmin({ 
            collapseWhitespace: true,
            conservativeCollapse: true,
            removeComments: true,
            minifyJS: true
        })))
        .pipe(gulp.dest('./build/'));

    gulp.src('assets/html/form_handler/*')
        .pipe(gulp.dest('./build/form_handler/'));

    gulp.src('assets/html/.htaccess')
        .pipe(gulp.dest('./build/'));

    gulp.src('assets/html/favicon.ico')
        .pipe(gulp.dest('./build/'));

    browserSync.reload();
});

// Webpack
gulp.task('js', function () {
    return gulp.src('assets/js/*.js')
        .pipe(webpack({
            mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
            output: {
                filename: 'main.js',
            },
            module: {
                rules: [{
                    test: /three\/examples\/js/,
                    use: 'imports-loader?THREE=three'
                }]
            },
            optimization: {
                minimizer: [
                    new ClosurePlugin({
                        mode: 'AGGRESSIVE_BUNDLE'
                    }, {
                        // compiler flags here
                        //
                        // for debuging help, try these:
                        //
                        // formatting: 'PRETTY_PRINT'
                        // debug: true,
                        // renaming: false
                    })
                ]
            },
            resolve: {
                alias: {
                    'three-examples': path.join(__dirname, './node_modules/three/examples/js')
                },
            },
        }, compiler, function (err, stats) {
            console.log(err);
        }))
        .on('error', handleErrors)
        //Three.JS is full of warning, useful for development but not needed in prod
        .pipe(gulpif(process.env.NODE_ENV === 'production', replace(/window\.console\.(?:warn|log)\(\"[a-z\s\.\:\(\)\d\']+?\"\)/gi, 'console.log("3WARN")')))
        .pipe(gulp.dest('./build/assets/js/'));
});

// Copy static files
gulp.task('copy-3d', function () {
    gulp.src(['assets/3d/*'])
        .pipe(gulp.dest('./build/assets/3d/'));

    browserSync.reload();
});

gulp.task('copy-img', function () {
    gulp.src(['assets/img/*'])
        .pipe(gulp.dest('./build/assets/img/'));

    browserSync.reload();
});



function handleErrors(error) {
    if (process.env.NODE_ENV === 'production') {
        console.log(error);
        process.exit(1);
    } else {
        console.error(error.message);
        browserSync.notify(error.message, 3000); // Display error in the browser
        this.emit('end');
    }

}